package src.task7.service.concrete;

import src.task7.bean.Login;
import src.task7.entity.User;
import src.task7.service.abstracts.UserService;

public class AccountManager {
    private UserService userService;
    private User loginedUser = null;
    private AccountManager accountManager = null;

    private AccountManager() {
    }

    public AccountManager getInstance(){
        if(accountManager == null) {
            accountManager = new AccountManager();
            userService = new UserManager();
        }
        return  accountManager;
    }

    public User login(Login login){
        if(loginedUser==null)
            loginedUser=new User();

        loginedUser = userService.getUserByUsernameAndPassword(login.getUserName(), login.getPassword());

        return loginedUser;
    }

    public void logout(){
        loginedUser = null;
    }
}