package src.task6;


import java.util.*;

public class Main {

    public static void main(String[] args) {
        Queue<Customer> customers = new PriorityQueue<>(Comparator.comparingInt(Customer::getAge));

        Task.addCustomer(customers);
    }



}
