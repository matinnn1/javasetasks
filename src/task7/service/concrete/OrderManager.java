package src.task7.service.concrete;

import src.task7.entity.Order;
import src.task7.entity.OrderLine;
import src.task7.entity.User;
import src.task7.repository.abstracts.OrderLineRepository;
import src.task7.repository.abstracts.OrderRepository;
import src.task7.repository.jdbc.mysql.OrderLineRepositoryImpl;
import src.task7.repository.jdbc.mysql.OrderRepositoryImpl;
import src.task7.service.abstracts.OrderService;
import src.task7.service.abstracts.UserService;

import java.util.List;

public class OrderManager implements OrderService {
    private OrderRepository orderRepository;
    private OrderLineRepository orderLineRepository;
    private UserService userService;

    public OrderManager(){
        orderRepository = new OrderRepositoryImpl();
        orderLineRepository=new OrderLineRepositoryImpl();
        userService = new UserManager();
    }

    @Override
    public List<Order> getAllOrders() {
        List<Order> orders = orderRepository.getAllOrders();

        for (Order order : orders){
            User customer = userService.getUserById(order.getCustomerId());
            User worker = userService.getUserById(order.getWorkerId());
            order.setCustomer(customer);
            order.setWorker(worker);
            order.setOrderLines(orderLineRepository.getAllOrderLinesByOrderId(order.getId()));
        }

        return orders;
    }

    @Override
    public Order getOrderById(int id) {
        Order order = orderRepository.getOrderById(id);

        order.setCustomer(userService.getUserById(order.getCustomerId()));
        order.setWorker( userService.getUserById(order.getWorkerId()));
        order.setOrderLines(orderLineRepository.getAllOrderLinesByOrderId(order.getId()));

        return order;
    }

    @Override
    public List<Order> getAllOrdersByStatus(int orderStatusId) {
        List<Order> orders = orderRepository.getAllOrdersByStatus(orderStatusId);

        for (Order order : orders){
            User customer = userService.getUserById(order.getCustomerId());
            User worker = userService.getUserById(order.getWorkerId());
            order.setCustomer(customer);
            order.setWorker(worker);
            order.setOrderLines(orderLineRepository.getAllOrderLinesByOrderId(order.getId()));
        }

        return orders;
    }

    @Override
    public List<Order> getAllOrdersByCustomerId(int customerId) {
        List<Order> orders = orderRepository.getAllOrdersByCustomerId(customerId);

        for (Order order : orders){
            User customer = userService.getUserById(order.getCustomerId());
            User worker = userService.getUserById(order.getWorkerId());
            order.setCustomer(customer);
            order.setWorker(worker);
            order.setOrderLines(orderLineRepository.getAllOrderLinesByOrderId(order.getId()));
        }

        return orders;
    }

    @Override
    public List<Order> getAllOrdersByWorkerId(int workerId) {
        List<Order> orders = orderRepository.getAllOrdersByWorkerId(workerId);

        for (Order order : orders){
            User customer = userService.getUserById(order.getCustomerId());
            User worker = userService.getUserById(order.getWorkerId());
            order.setCustomer(customer);
            order.setWorker(worker);
            order.setOrderLines(orderLineRepository.getAllOrderLinesByOrderId(order.getId()));
        }

        return orders;
    }

    @Override
    public void addOrder(Order order) {
        orderRepository.addOrder(order);
    }

    @Override
    public void updateOrder(Order order) {
        orderRepository.updateOrder(order);
    }


}
