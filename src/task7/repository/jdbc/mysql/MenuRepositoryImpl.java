package src.task7.repository.jdbc.mysql;

import src.task7.entity.Menu;
import src.task7.entity.User;
import src.task7.repository.abstracts.MenuRepository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class MenuRepositoryImpl implements MenuRepository {
    @Override
    public List<Menu> GetAll() {
        String query ="select * from menus";

        List<Menu> menus = new ArrayList<>();

        try(Connection connection = connectToMySqlDb("task7");) {

            Statement statement=connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()){
                menus.add(getMenu(resultSet));
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return menus;
    }

    @Override
    public Menu GetById(int id) {
        String query ="select * from menus where id="+id;

        Menu menu = null;

        try(Connection connection = connectToMySqlDb("task7");) {

            Statement statement=connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()){
                menu = getMenu(resultSet);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return menu;
    }


    private Menu getMenu(ResultSet resultSet) throws SQLException {
        Menu menu = new Menu();
        menu.setId(resultSet.getInt("id"));
        menu.setComposition(resultSet.getString("composition"));
        menu.setDescription(resultSet.getString("description"));
        menu.setPrice(resultSet.getFloat("price"));

        return menu;
    }
}
