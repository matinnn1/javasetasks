package src.task7.entity;

import java.util.Objects;

public class Menu {
    private int id;
    private String description;

    private String composition;
    private float price;

    public Menu() {
    }

    public Menu(int id, String description, String composition, float price) {
        this.id = id;
        this.description = description;
        this.composition = composition;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getComposition() {
        return composition;
    }

    public void setComposition(String composition) {
        this.composition = composition;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Menu menu = (Menu) o;
        return id == menu.id && Float.compare(menu.price, price) == 0 && Objects.equals(description, menu.description) && Objects.equals(composition, menu.composition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, composition, price);
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", composition='" + composition + '\'' +
                ", price=" + price +
                '}';
    }
}
