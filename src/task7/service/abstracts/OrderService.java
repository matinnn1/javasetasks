package src.task7.service.abstracts;

import src.task7.entity.Order;

import java.util.List;

public interface OrderService {
    List<Order> getAllOrders();
    Order getOrderById(int id);
    List<Order> getAllOrdersByStatus(int orderStatusId);
    List<Order> getAllOrdersByCustomerId(int customerId);
    List<Order> getAllOrdersByWorkerId(int workerId);
    void addOrder(Order order);
    void updateOrder(Order order);
}
