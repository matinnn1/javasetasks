package src.task3;


import java.io.IOException;
import java.util.Scanner;

public class Main {
    static Student[] students=new Student[]{};
    public static void main(String[] args) throws IOException {
        writeCode();
    }

    public static void writeCode(){

        Scanner scanner = new Scanner(System.in);

        System.out.printf("choice: ");
        int choice = scanner.nextInt();

        while(choice == 1 || choice == 2){

            if(choice==1) {
                Student student = createtudent(scanner);
                addStudentToArray(student);

                System.out.println(student +" --> Succesfully added");
            }

            else if(choice==2){
                showAllStudents(students);
            }

            System.out.printf("choice: ");
            choice = scanner.nextInt();
        }

        if(choice==3){
            System.exit(0);
        }

        else{
            System.out.printf("choice: ");
            choice = scanner.nextInt();
        }
    }

    public static Student createtudent(Scanner scanner){
        // Student student=new Student("Metin","Nuri","Ingress",(byte)24);

        Student student= new Student();

        System.out.printf("Student Name: ");
        student.setName(scanner.next());

        System.out.printf("Student Surname: ");
        student.setSurname(scanner.next());

        System.out.printf("Student age: ");
        student.setAge(scanner.nextByte());

        System.out.printf("Student Course Name: ");

        student.setCourseName(scanner.next());
        return student;
    }

    public static void addStudentToArray(Student student){
        Student[] oldStudents = students;

        students = new Student[students.length+1];

        if(students.length==1){
            students[0]=student;
        }

        else {

            for (int i = 0; i < oldStudents.length; i++) {
                students[i] = oldStudents[i];
            }
            students[oldStudents.length] = student;
        }
    }

    public static void showAllStudents(Student[] students){
        for (int i = 0; i < students.length; i++) {
            System.out.println((i+1)+". "+students[i]);
        }
    }
}

