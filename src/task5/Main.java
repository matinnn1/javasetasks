package src.task5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.printf("Write your letter: ");
        String str = scanner.nextLine();

        Vowel.countVowels(str);
        Reverse.reverseStr(str);
        LetterCount.countLetter(str);
        Polidrome.checkPolindrome(str);
    }
}