package src.task7.repository.abstracts;

import com.mysql.cj.protocol.x.FetchDoneMoreResults;
import src.task7.entity.Order;
import src.task7.entity.OrderStatus;
import src.task7.entity.User;

import java.util.List;

public interface OrderRepository extends DbConnection{
    List<Order> getAllOrders();
    Order getOrderById(int id);
    List<Order> getAllOrdersByStatus(int orderStatusId);
    List<Order> getAllOrdersByCustomerId(int customerId);
    List<Order> getAllOrdersByWorkerId(int workerId);
    void addOrder(Order order);
    void updateOrder(Order order);
}