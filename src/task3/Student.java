package src.task3;


public class Student {
    private String name;
    private String surname;
    private String courseName;

    private byte age;

    public String getCourseName() {
        return courseName;
    }

    public Student(){

    }
    public Student(String name, String surname, String courseName, byte age) {
        this.name = name;
        this.surname = surname;
        this.courseName = courseName;
        this.age = age;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public byte getAge() {
        return age;
    }

    public void setAge(byte age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", courseName='" + courseName + '\'' +
                ", age=" + age +
                '}';
    }
}

