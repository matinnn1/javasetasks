package src.task7.service.abstracts;

import src.task7.entity.User;

import java.util.List;

public interface UserService {
    List<User> getAllUsers();
    User getUserById(int id);
    void addUser(User user);
    User getUserByUsernameAndPassword(String username, String password);
    User getUserByUsername(String username);
}
