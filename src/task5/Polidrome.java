package src.task5;

public class Polidrome {
    public  static void checkPolindrome(String str) {
        String result = "";
        String strLowerCase = str.toLowerCase();

        for (int i = 0; i < strLowerCase.length(); i++) {
            if (strLowerCase.charAt(i) != strLowerCase.charAt((strLowerCase.length() - 1) - i)) {
                result = "not";
                break;
            }
        }

        System.out.println("'"+str+"' is "+result+" polindrome.");
    }
}
