package src.task7.repository.jdbc.mysql;

import src.task7.entity.Order;
import src.task7.entity.OrderStatus;
import src.task7.entity.User;
import src.task7.repository.abstracts.OrderRepository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class OrderRepositoryImpl implements OrderRepository {

    @Override
    public List<Order> getAllOrders() {
        String query ="select \n" +
                "\t ord.id\n" +
                "\t,ord.orderedDateTime \n" +
                "\t,ord.customerId \n" +
                "\t,ord.workerId\n" +
                "\t,ord.statusId\n" +
                "\t,ordst.name status\n" +
                "from orders ord\n" +
                "left join orderstatus ordst on ord.statusId=ordst.id ";

        List<Order> orders = new ArrayList<>();

        try(Connection connection = connectToMySqlDb("task7");) {

            Statement statement=connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()){
                orders.add(getOrder(resultSet));
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return orders;
    }

    @Override
    public Order getOrderById(int id) {
        String query ="select \n" +
                "\t ord.id\n" +
                "\t,ord.orderedDateTime \n" +
                "\t,ord.customerId \n" +
                "\t,ord.workerId\n" +
                "\t,ord.statusId\n" +
                "\t,ordst.name status\n" +
                "from orders ord\n" +
                "left join orderstatus ordst on ord.statusId=ordst.id " +
                "where ord.id = " + id;

        Order order = null;

        try(Connection connection = connectToMySqlDb("task7");) {

            Statement statement=connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()){
                order = getOrder(resultSet);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return order;
    }

    @Override
    public List<Order> getAllOrdersByStatus(int orderStatusId) {
        String query ="select \n" +
                "\t ord.id\n" +
                "\t,ord.orderedDateTime \n" +
                "\t,ord.customerId \n" +
                "\t,ord.workerId\n" +
                "\t,ord.statusId\n" +
                "\t,ordst.name status\n" +
                "from orders ord\n" +
                "left join orderstatus ordst on ord.statusId=ordst.id " +
                "where ord.statusId = "+orderStatusId;

        List<Order> orders = new ArrayList<>();

        try(Connection connection = connectToMySqlDb("task7");) {

            Statement statement=connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()){
                orders.add(getOrder(resultSet));
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return orders;
    }

    @Override
    public List<Order> getAllOrdersByCustomerId(int customerId) {
        String query ="select \n" +
                "\t ord.id\n" +
                "\t,ord.orderedDateTime \n" +
                "\t,ord.customerId \n" +
                "\t,ord.workerId\n" +
                "\t,ord.statusId\n" +
                "\t,ordst.name status\n" +
                "from orders ord\n" +
                "left join orderstatus ordst on ord.statusId=ordst.id " +
                "where ord.customerId = "+customerId;

        List<Order> orders = new ArrayList<>();

        try(Connection connection = connectToMySqlDb("task7");) {

            Statement statement=connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()){
                orders.add(getOrder(resultSet));
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return orders;
    }

    @Override
    public List<Order> getAllOrdersByWorkerId(int workerId) {
        String query ="select \n" +
                "\t ord.id\n" +
                "\t,ord.orderedDateTime \n" +
                "\t,ord.customerId \n" +
                "\t,ord.workerId\n" +
                "\t,ord.statusId\n" +
                "\t,ordst.name status\n" +
                "from orders ord\n" +
                "left join orderstatus ordst on ord.statusId=ordst.id " +
                "where ord.workerId = "+workerId;

        List<Order> orders = new ArrayList<>();

        try(Connection connection = connectToMySqlDb("task7");) {

            Statement statement=connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()){
                orders.add(getOrder(resultSet));
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return orders;
    }

    @Override
    public void addOrder(Order order) {

        String query ="INSERT INTO orders\n" +
                "(" +
                    "customerId\n" +
                    "\t,statusId\n" +
                    "\t,workerId\n" +
                    "\t,orderedDateTime\n" +
                ")\n" +
                "values\n" +
                    "(\n" +
                    "\t "+order.getCustomerId()+"\n" +
                    "\t,"+order.getStatus().getId()+"\n" +
                    "\t,0\n" +
                    "\t,now()\n" +
                ")";


        try {
            Connection connection = connectToMySqlDb("task7");

            Statement statement=connection.createStatement();
            statement.executeUpdate(query);

            connection.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void updateOrder(Order order) {
        String query ="UPDATE orders SET \n" +
                            "\t customerId="+order.getCustomerId()+"\n" +
                            "\t,statusId="+order.getStatusId()+"\n" +
                            "\t,workerId="+order.getWorkerId()+"\n" +
                            "\t,orderedDateTime=now()\n" +
                      "WHERE id="+order.getId();


        try {
            Connection connection = connectToMySqlDb("task7");

            Statement statement=connection.createStatement();
            statement.executeUpdate(query);

            connection.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Order getOrder(ResultSet resultSet) throws SQLException {
        Order order = new Order ();
        order.setId(resultSet.getInt("id"));
        order.setCustomerId(resultSet.getInt("customerId"));
        order.setWorkerId(resultSet.getInt("workerId"));
        order.setOrderedDateTime((LocalDateTime) resultSet.getObject("orderedDateTime"));

        order.setStatus(getOrderStatus(resultSet));

        return order;
    }

    private OrderStatus getOrderStatus(ResultSet resultSet) throws SQLException {
        OrderStatus orderStatus=new OrderStatus();
        orderStatus.setId(resultSet.getInt("statusId"));
        orderStatus.setName(resultSet.getString("status"));

        return orderStatus;
    }
}
