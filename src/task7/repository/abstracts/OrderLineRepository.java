package src.task7.repository.abstracts;

import src.task7.entity.OrderLine;

import java.util.List;

public interface OrderLineRepository extends DbConnection{
    List<OrderLine> getAllOrderLinesByOrderId(int orderId);
    void addOrderLine(OrderLine orderLine);
}
