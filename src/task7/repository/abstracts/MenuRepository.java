package src.task7.repository.abstracts;

import src.task7.entity.Menu;

import java.util.List;

public interface MenuRepository extends DbConnection {
    List<Menu> GetAll();
    Menu GetById(int id);
}
