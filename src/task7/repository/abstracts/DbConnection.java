package src.task7.repository.abstracts;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public interface DbConnection {
    default Connection connectToMySqlDb(String dbName) throws SQLException {
        String url="jdbc:mysql://localhost:3306/"+dbName;
        String sqlUser="root";
        String password="1511";

        return DriverManager.getConnection(url,sqlUser,password);
    }
}
