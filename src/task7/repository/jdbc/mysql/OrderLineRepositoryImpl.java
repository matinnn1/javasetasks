package src.task7.repository.jdbc.mysql;

import src.task7.entity.Order;
import src.task7.entity.OrderLine;
import src.task7.repository.abstracts.OrderLineRepository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class OrderLineRepositoryImpl implements OrderLineRepository {
    @Override
    public List<OrderLine> getAllOrderLinesByOrderId(int orderId) {
        String query ="SELECT \n" +
                "\t id\n" +
                "\t,orderId\n" +
                "\t,menuId\n" +
                "\t,quantity\n" +
                "FROM orderlines\n" +
                "where orderId = "+orderId;

        List<OrderLine> orderLines = new ArrayList<>();

        try(Connection connection = connectToMySqlDb("task7");) {

            Statement statement=connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()){
                OrderLine orderLine=new OrderLine();

                orderLine.setId(resultSet.getInt("id"));
                orderLine.setOrderId(resultSet.getInt("orderId"));
                orderLine.setQuantity(resultSet.getDouble("quantity"));
                orderLine.setMenuId(resultSet.getInt("menuId"));

                orderLines.add(orderLine);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return orderLines;
    }

    @Override
    public void addOrderLine(OrderLine orderLine) {

        String query ="INSERT INTO orderlines\n" +
                    "(\n" +
                        "\t orderId\n" +
                        "\t,menuId\n" +
                        "\t,quantity)\n" +
                    "values\n" +
                    "(\n" +
                        "\t "+orderLine.getOrderId()+"\n" +
                        "\t,"+orderLine.getMenuId()+"\n" +
                        "\t,"+orderLine.getQuantity()+"\n" +
                    ")";

        try {
            Connection connection = connectToMySqlDb("task7");

            Statement statement=connection.createStatement();
            statement.executeUpdate(query);

            connection.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}
