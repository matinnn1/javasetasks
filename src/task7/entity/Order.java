package src.task7.entity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class Order {
    public Order() {
    }

    public Order(int id, int customerId, User customer, int statusId, OrderStatus status, int workerId, User worker, LocalDateTime orderedDateTime, List<OrderLine> orderLines) {
        this.id = id;
        this.customerId = customerId;
        this.customer = customer;
        this.statusId = statusId;
        this.status = status;
        this.workerId = workerId;
        this.worker = worker;
        this.orderedDateTime = orderedDateTime;
        this.orderLines = orderLines;
    }

    public Order(int id, int customerId, User customer, int statusId, OrderStatus status, int workerId, User worker, LocalDateTime orderedDateTime) {
        this.id = id;
        this.customerId = customerId;
        this.customer = customer;
        this.statusId = statusId;
        this.status = status;
        this.workerId = workerId;
        this.worker = worker;
        this.orderedDateTime = orderedDateTime;
    }

    private int id;

    private int customerId;
    private User customer;

    private int statusId;
    private OrderStatus status;

    private  int workerId;
    private User worker;
    private LocalDateTime orderedDateTime;

    private List<OrderLine> orderLines;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getCustomer() {
        return customer;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getWorkerId() {
        return workerId;
    }

    public List<OrderLine> getOrderLines() {
        return orderLines;
    }

    public void setOrderLines(List<OrderLine> orderLines) {
        this.orderLines = orderLines;
    }

    public void setWorkerId(int workerId) {
        this.workerId = workerId;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public User getWorker() {
        return worker;
    }

    public void setWorker(User worker) {
        this.worker = worker;
    }

    public LocalDateTime getOrderedDateTime() {
        return orderedDateTime;
    }

    public void setOrderedDateTime(LocalDateTime orderedDateTime) {
        this.orderedDateTime = orderedDateTime;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id && customerId == order.customerId && statusId == order.statusId && workerId == order.workerId && Objects.equals(customer, order.customer) && Objects.equals(status, order.status) && Objects.equals(worker, order.worker) && Objects.equals(orderedDateTime, order.orderedDateTime) && Objects.equals(orderLines, order.orderLines);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, customerId, customer, statusId, status, workerId, worker, orderedDateTime, orderLines);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", customerId=" + customerId +
                ", customer=" + customer +
                ", statusId=" + statusId +
                ", status=" + status +
                ", workerId=" + workerId +
                ", worker=" + worker +
                ", orderedDateTime=" + orderedDateTime +
                ", orderLines=" + orderLines +
                '}';
    }
}
