package src.task2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        javaLoopsProblem();
    }

    private static void javaLoopsProblem() {
        Scanner in = new Scanner(System.in);
        System.out.print("T=");
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            System.out.print("a=");
            int a = in.nextInt();
            System.out.print("b=");
            int b = in.nextInt();

            System.out.print("n=");
            int n = in.nextInt();

            for (int j = 0; j < n; j++) {
                a += Math.pow(2, j) * b;
                System.out.printf(a + " ");
            }
            System.out.println();
        }

    }
}
