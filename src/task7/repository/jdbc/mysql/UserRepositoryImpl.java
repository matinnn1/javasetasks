package src.task7.repository.jdbc.mysql;

import src.task7.entity.User;
import src.task7.repository.abstracts.UserRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryImpl implements UserRepository {
    @Override
    public List<User> getAllUsers() {
        String query ="select * from users";

        List<User> users = new ArrayList<>();

        try(Connection connection = connectToMySqlDb("task7");) {

            Statement statement=connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()){
                users.add(getUser(resultSet));
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return users;
    }

    @Override
    public User getUserById(int id) {
        String query ="select * from users where id=" +id;

        User user=null;

        try {
            Connection connection = connectToMySqlDb("task7");

            Statement statement=connection.createStatement();
            ResultSet resultSet= statement.executeQuery(query);

            while (resultSet.next()){
                user = getUser(resultSet);
            }

            connection.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return user;
    }

    @Override
    public void addUser(User user) {

        String query ="insert into Users (\n" +
                "    name,\n" +
                "    surname,\n" +
                "    username,\n" +
                "    password\n" +
                ") \n" +
                "\tvalues(\n" +
                "\t'"+user.getName()+"',\n" +
                "\t'"+user.getSurname()+"',\n" +
                "\t'"+user.getUserName()+"',\n" +
                "\t'"+user.getPassword()+"'\n" +
                ")";


        try {
            Connection connection = connectToMySqlDb("task7");

            Statement statement=connection.createStatement();
            statement.executeUpdate(query);

            connection.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public User getUserByUsernameAndPassword(String username, String password) {
        String query ="select * from users where username='" +username+"' and password = '"+password+"'";

        User user = null;

        try {
            Connection connection = connectToMySqlDb("task7");

            Statement statement=connection.createStatement();
            ResultSet resultSet= statement.executeQuery(query);

            while (resultSet.next()){
                user = getUser(resultSet);
            }

            connection.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return user;
    }

    @Override
    public User getUserByUsername(String username) {
        String query ="select * from users where username='" +username+"'";

        User user = null;

        try {
            Connection connection = connectToMySqlDb("task7");

            Statement statement=connection.createStatement();
            ResultSet resultSet= statement.executeQuery(query);

            while (resultSet.next()){
                user = getUser(resultSet);
            }

            connection.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return user;
    }

    @Override
    public void addUserRole(int userId, int roleId) {
        String query ="\n" +
                "insert into UsersRoles (\n" +
                "    userId,\n" +
                "    roleId \n" +
                ") \n" +
                "\tvalues(\n" +
                "\t"+userId+",\n" +
                "\t"+roleId+"\n" +
                "\t\n" +
                ")";


        try {
            Connection connection = connectToMySqlDb("task7");

            Statement statement=connection.createStatement();
            statement.executeUpdate(query);

            connection.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private User getUser(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt("id"));
        user.setName(resultSet.getString("name"));
        user.setSurname(resultSet.getString("surname"));
        user.setUserName(resultSet.getString("username"));
        user.setPassword(resultSet.getString("password"));

        return user;
    }
}


