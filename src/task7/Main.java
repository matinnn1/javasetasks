package src.task7;

import src.task7.entity.User;
import src.task7.repository.abstracts.UserRepository;
import src.task7.repository.jdbc.mysql.UserRepositoryImpl;
import src.task7.service.abstracts.MenuService;
import src.task7.service.abstracts.OrderService;
import src.task7.service.abstracts.UserService;
import src.task7.service.concrete.MenuManager;
import src.task7.service.concrete.OrderManager;
import src.task7.service.concrete.UserManager;

public class Main {
    public static void main(String[] args) {
        OrderService orderService=new OrderManager();
        System.out.println(orderService.getAllOrders());
    }

    private static void menuManager() {
        MenuService menuService = new MenuManager();
        System.out.println(menuService.GetById(1));
    }

    private static void userManager(){
        UserService userService = new UserManager();
        //User user = new User("Metin","Nuri","nurimetin98","12345");
        //userService.addUser(user);
        //System.out.println(userService.getUserByUsernameAndPassword("nurimetin98","12345"));
        //System.out.println(userService.getAllUsers());
    }
}