package src.task6;

import java.util.Queue;
import java.util.Scanner;

public class Task {

    public static void addCustomer(Queue<Customer> customers){
        Scanner scanner = new Scanner(System.in);
        String str = "";

        while (!str.equals("exit")){
            Customer createdCustomer = createCustomer(scanner);

            System.out.printf("Write 'add' if you want to add customer to queue : ");

            scanner = new Scanner(System.in);
            str=scanner.nextLine();

            if(str.equals("add")){
                customers.offer(createdCustomer);
                System.out.println("Customer successfully added!");
            }

            else
                System.out.println("Customer don't added!");

            System.out.println(customers);

            System.out.printf("Write 'service' if you want to provide customer service, if you want to exit, write 'exit':");
            scanner = new Scanner(System.in);
            str=scanner.nextLine();

            if(str.equals("exit")){
                System.exit(0);
            }

            else if (str.equals("service")) {
                System.out.println("Serviced : "+customers.poll());
                System.out.println("Next : "+customers.peek());
            }
        }
    }

    public static Customer createCustomer(Scanner scanner){

        Customer customer= new Customer();

        System.out.printf("Customer Name: ");
        customer.setName(scanner.nextLine());

        System.out.printf("Customer Surname: ");
        customer.setSurname(scanner.nextLine());

       // System.out.printf("Customer age: ");
        //customer.setAge(scanner.nextByte());
        checkAge(scanner, customer);

        return customer;
    }

    private static void checkAge(Scanner scanner, Customer customer){
        byte age=0;
        System.out.printf("Customer age: ");
        try{
            age=scanner.nextByte();
            customer.setAge(age);
        }
        catch (Exception ex){
            checkAge(scanner,customer);
        }
    }
}
