package src.task7.entity;

import java.util.List;

public class User {

    public User() {

    }

    public User(int id, String name, String surname, String userName, String password) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.userName = userName;
        this.password = password;
    }

    public User(String name, String surname, String userName, String password) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.userName = userName;
        this.password = password;
    }



    private int id;
    private String name;
    private String surname;
    private String userName;
    private String password;

    private List<Role> userRoles;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<Role> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<Role> userRoles) {
        this.userRoles = userRoles;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
