package src.task7.service.concrete;

import src.task7.entity.User;
import src.task7.repository.abstracts.RoleRepository;
import src.task7.repository.abstracts.UserRepository;
import src.task7.repository.jdbc.mysql.RoleRepositoryImpl;
import src.task7.repository.jdbc.mysql.UserRepositoryImpl;
import src.task7.service.abstracts.UserService;
import java.util.List;

public class UserManager implements UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    public UserManager(){
        userRepository = new UserRepositoryImpl();
        roleRepository = new RoleRepositoryImpl();
    }

    @Override
    public List<User> getAllUsers() {
        List<User> users = userRepository.getAllUsers();

        for(User user : users){
            user.setUserRoles(roleRepository.getByUserId(user.getId()));
        }

        return users;
    }

    @Override
    public User getUserById(int id) {
        User user = userRepository.getUserById(id);

        user.setUserRoles(roleRepository.getByUserId(user.getId()));

        return user;
    }

    @Override
    public void addUser(User user) {
        userRepository.addUser(user);

        User createdUser= userRepository.getUserByUsername(user.getUserName());

        if(createdUser!=null)
            userRepository.addUserRole(createdUser.getId(),2);

        createdUser.setUserRoles(roleRepository.getByUserId(user.getId()));
    }

    @Override
    public User getUserByUsernameAndPassword(String username, String password) {
        User user = userRepository.getUserByUsernameAndPassword(username,password);

        user.setUserRoles(roleRepository.getByUserId(user.getId()));

        return user;
    }

    @Override
    public User getUserByUsername(String username) {
        User user = userRepository.getUserByUsername(username);

        user.setUserRoles(roleRepository.getByUserId(user.getId()));

        return user;
    }
}