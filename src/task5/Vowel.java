package src.task5;

import java.util.Scanner;

public class Vowel {
    public static void countVowels(String str)
    {
        int upperCaseVowelCount=0;
        int lowerCaseVowelCount=0;

        if(str.isBlank() || str.isEmpty())
            System.out.println("string is empty");

        else {
            for(char element : str.toCharArray()){
                if(isLowerCaseVowel(element))
                    lowerCaseVowelCount++;

                else if(isUpperCaseVowel(element))
                    upperCaseVowelCount++;
            }

            System.out.println("Upper Case Vowel Count: "+upperCaseVowelCount);
            System.out.println("Lower Case Vowel Count: "+lowerCaseVowelCount);
            System.out.println("Total Vowel Count: "+(upperCaseVowelCount+lowerCaseVowelCount));

        }
    }

    public static boolean isLowerCaseVowel(char character){
        if(character=='a' || character=='ı' || character=='o' || character=='u' || character=='e' || character=='ə' || character=='i' || character=='ö' ||character=='ü')
            return  true;

        else
            return false;
    }

    public static boolean isUpperCaseVowel(char character){
        if(character=='A' || character=='I' || character=='O' || character=='U' || character=='E' || character=='Ə' || character=='İ' || character=='Ö' ||character=='Ü')
            return  true;

        else
            return false;
    }
}
