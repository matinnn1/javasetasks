package src.task7.entity;

import java.util.Objects;

public class OrderLine {
    public OrderLine() {
    }

    public OrderLine(int id, int orderId, int menuId, Menu menu, double quantity) {
        this.id = id;
        this.orderId = orderId;
        this.menuId = menuId;
        this.menu = menu;
        this.quantity = quantity;
    }

    private   int id;
    private int orderId;
    private int menuId;
    private Menu menu;
    private double quantity;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderLine orderLine = (OrderLine) o;
        return id == orderLine.id && orderId == orderLine.orderId && menuId == orderLine.menuId && Double.compare(orderLine.quantity, quantity) == 0 && Objects.equals(menu, orderLine.menu);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, orderId, menuId, menu, quantity);
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", orderId=" + orderId +
                ", menuId=" + menuId +
                ", menu=" + menu +
                ", quantity=" + quantity +
                '}';
    }
}
