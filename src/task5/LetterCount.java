package src.task5;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LetterCount {
    public static void countLetter(String str){
        int count=0;
        for(char element : str.toCharArray()){
             if(element!='\u0020')
                 count++;
        }
        System.out.println("'"+str+"' letter count is "+ count);
    }
}