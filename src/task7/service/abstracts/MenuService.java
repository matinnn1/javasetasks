package src.task7.service.abstracts;

import src.task7.entity.Menu;

import java.util.List;

public interface MenuService {
    List<Menu> GetAll();
    Menu GetById(int id);
}
