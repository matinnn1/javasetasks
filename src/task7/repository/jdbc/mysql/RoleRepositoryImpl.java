package src.task7.repository.jdbc.mysql;

import src.task7.entity.Role;
import src.task7.entity.User;
import src.task7.repository.abstracts.RoleRepository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class RoleRepositoryImpl implements RoleRepository {
    @Override
    public Role getById(int id) {
        String query ="select * from roles where id = "+id;

        Role role=null;

        try {
            Connection connection = connectToMySqlDb("task7");

            Statement statement=connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()){
                role = getRole(resultSet);
            }

            connection.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return role;
    }

    @Override
    public List<Role> getByUserId(int userId) {
        String query ="select \n" +
                "\trl.id, \n" +
                "\trl.name  \n" +
                "from usersroles usrl\n" +
                "left join roles rl on usrl.roleId = rl.id\n" +
                "where usrl.userId = "+userId;

       List<Role> roles=new ArrayList<>();

        try {
            Connection connection = connectToMySqlDb("task7");

            Statement statement=connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()){
                roles.add(getRole(resultSet));
            }

            connection.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return roles;
    }

    private Role getRole(ResultSet resultSet) throws SQLException {
        Role role = new Role();
        role.setId(resultSet.getInt("id"));
        role.setName(resultSet.getString("name"));

        return role;
    }
}
