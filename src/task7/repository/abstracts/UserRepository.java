package src.task7.repository.abstracts;

import src.task7.entity.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public interface UserRepository extends DbConnection{
    List<User> getAllUsers();
    User getUserById(int id);
    void addUser(User user);
    User getUserByUsernameAndPassword(String username, String password);
    User getUserByUsername(String username);
    void addUserRole(int userId, int roleId);
}
