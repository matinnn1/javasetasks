package src.task7.repository.abstracts;

import src.task7.entity.Role;

import java.util.List;

public interface RoleRepository extends DbConnection{
    Role getById(int id);
    List<Role> getByUserId(int userId);
}
