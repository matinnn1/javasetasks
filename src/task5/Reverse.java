package src.task5;

import java.util.Scanner;

public class Reverse {
    public static void reverseStr(String str){
        String result="";

        if(str.isBlank() || str.isEmpty())
            System.out.println("string is empty");

        else{
            for(int i=str.length()-1; i>=0; i--){
               result+= str.charAt(i);
            }
        }

        System.out.println("Reversed Letter is: "+result);
    }
}
