package src.task7.service.concrete;

import src.task7.entity.Menu;
import src.task7.repository.abstracts.MenuRepository;
import src.task7.repository.jdbc.mysql.MenuRepositoryImpl;
import src.task7.service.abstracts.MenuService;

import java.util.List;

public class MenuManager implements MenuService {
    private MenuRepository menuRepository;

    public MenuManager(){
        menuRepository = new MenuRepositoryImpl();
    }

    @Override
    public List<Menu> GetAll() {
        return menuRepository.GetAll();
    }

    @Override
    public Menu GetById(int id) {
        return menuRepository.GetById(id);
    }
}