package src.task6;

import java.util.Objects;

public class Customer {
    private String name;
    private String surname;
    private byte age;

    public Customer(){

    }

//    public Customer(String name, String surname, byte age) {
//        this.name = name;
//        this.surname = surname;
//        this.age = age;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public byte getAge() {
        return age;
    }

    public void setAge(byte age) {
        this.age = age;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Customer customer = (Customer) o;
//        return age == customer.age && Objects.equals(name, customer.name) && Objects.equals(surname, customer.surname);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(name, surname, age);
//    }

    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                '}';
    }





}
